FROM golang:1.10.2-alpine3.7 AS build
RUN apk --no-cache add gcc g++ make ca-certificates
WORKDIR /go/src/gitlab.com/Dokazhi/g1-software-task
COPY . .
RUN go build -o /go/bin/linux_app
RUN env GOARCH=amd64 GOOS=windows go build -o /go/bin/win_app
FROM alpine:3.7
WORKDIR /usr/bin
COPY --from=build /go/bin .
#CHANGE THIS PORT FOR PREFER
EXPOSE 9901 

CMD ["./linux_app -db-port '3306' -image-path 'image' -server '192.168.1.183' -port '9999' -db-server '1232'"]
