package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
	"strconv"
)

var imagesPath, server, dbServer string
var port, dbPort int

// type imageHandler struct{}

// func (h imageHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
// 	fmt.Fprintf(w, "Hello %s", r.URL.Path)
// }

func main() {

	imagesPath := flag.String("image-path", "", "path where to serve iamges")
	server := flag.String("server", "", "domain/ip-address")
	port := flag.Int("port", 9901, "which port to connect")
	dbServer := flag.String("db-server", "", "ip of db")
	dbPort := flag.Int("db-port", 0, "3306 standart of mysql")
	flag.Parse()
	log.Println(*imagesPath, *server, *port, *dbPort, *dbServer)
	if *imagesPath == "" || *server == "" || *port == 0 || *dbServer == "" ||
		*dbPort == 0 {
		fmt.Println("NOT ENOUGH ARGS")
		return
	}
	if err := os.MkdirAll(*imagesPath+"/parts", os.ModePerm); err != nil {
		panic(err)
	}
	h := http.NewServeMux()

	h.HandleFunc("/images/", imageServe)
	h.HandleFunc("/images/:id/parts/:part_num", getPart)

	if err := http.ListenAndServe(*server+":"+strconv.Itoa(*port), h); err != nil {
		panic(err)
	}
}
