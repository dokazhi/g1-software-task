package main

import (
	"database/sql"
	"fmt"
	"image"
	"image/draw"
	"regexp"
	"strings"

	"io/ioutil"
	"log"
	"mime"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
	"time"

	_ "image/gif"
	_ "image/jpeg"
	"image/png"

	_ "github.com/mattn/go-sqlite3"
)

///GETTING 1 QUATER OF GIVING IMAGE
func makePart(source image.Image, filename string, x int, y int, n int) (string, error) {
	part := image.NewRGBA(image.Rect(0, 0, source.Bounds().Max.X/2, source.Bounds().Max.Y/2))
	draw.Draw(part, part.Bounds(), source, image.Point{
		X: x,
		Y: y,
	}, draw.Src)
	partName := imagesPath + "parts/part" + strconv.Itoa(n) + "-" + filename
	output, err := os.Create(partName)
	if err != nil {
		return "", err
	}
	err = png.Encode(output, part)
	if err != nil {
		return "", err
	}
	return partName, nil
}

///  /images handler
func imageServe(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		file, header, err := r.FormFile("image")
		if err != nil {
			fmt.Fprintln(w, "No image, sorry")
			return
		}
		defer file.Close()
		fileBytes, err := ioutil.ReadAll(file)
		fileType := http.DetectContentType(fileBytes)
		if fileType != "image/jpeg" && fileType != "image/jpg" && fileType != "image/gif" && fileType != "image/png" {
			fmt.Fprintln(w, "File type is not acceptable")
			return

		}
		fileEnds, err := mime.ExtensionsByType(fileType)
		if err != nil {
			fmt.Fprintln(w, "Bad mime type, sorry")
			return
		}
		curTime := strconv.FormatInt(time.Now().UnixNano(), 10)
		newImageName := filepath.Join(imagesPath, curTime+fileEnds[0])
		newImage, err := os.Create(newImageName)
		if err != nil {
			fmt.Fprintln(w, "Can't create new image, sorry")
			return
		}
		defer newImage.Close()
		_, err = newImage.Write(fileBytes)
		if err != nil {
			fmt.Fprintln(w, "Can't write into new image,sorry")
			return
		}

		//IMAGE PLAYING

		readableImage, err := os.Open(newImageName)
		if err != nil {
			fmt.Fprintln(w, "Sorry server serving error")
			err = os.Remove(newImageName)
			if err != nil {
				fmt.Fprintln(w, "Something strange happens on server")
				return
			}
			return
		}
		defer readableImage.Close()
		log.Println(readableImage.Name())
		imageData, _, err := image.Decode(readableImage)
		if err != nil {
			err = os.Remove(newImageName)
			if err != nil {
				fmt.Fprintln(w, "Something strange happens on server")
				return
			}
			return
		}

		var parts []string
		part, err := makePart(imageData, curTime+fileEnds[0], 0, 0, 1)
		if err != nil {
			log.Println(err)
			fmt.Fprintln(w, "Sorry error on slicing")
			return
		}
		parts = append(parts, part)
		part, err = makePart(imageData, curTime+fileEnds[0], 0, imageData.Bounds().Max.Y/2, 2)
		if err != nil {
			log.Println(err)
			fmt.Fprintln(w, "Sorry error on slicing")
			return
		}
		parts = append(parts, part)
		part, err = makePart(imageData, curTime+fileEnds[0], imageData.Bounds().Max.X/2, 0, 3)
		if err != nil {
			log.Println(err)
			fmt.Fprintln(w, "Sorry error on slicing")
			return
		}
		parts = append(parts, part)
		part, err = makePart(imageData, curTime+fileEnds[0], imageData.Bounds().Max.X/2, imageData.Bounds().Max.Y/2, 4)
		if err != nil {
			log.Println(err)
			fmt.Fprintln(w, "Sorry error on slicing")
			return
		}
		parts = append(parts, part)
		log.Println(parts)
		//DATABASE CONNECTION
		db, err := sql.Open("sqlite3", "gsc.db")
		if err != nil {
			log.Println(err)
			err = os.Remove(newImageName)
			if err != nil {
				fmt.Fprintln(w, "Something strange happens on server")
				return
			}
			return
		}
		defer db.Close()
		// result, err := db.Exec("CREATE TABLE pictures( id INTEGER  PRIMARY KEY AUTOINCREMENT, path TEXT not null, origin TEXT not null, created_at TEXT NOT NULL)")
		transaction, err := db.Begin()
		if err != nil {
			fmt.Fprintln(w, "Error on transaction")
		}
		imageExec, err := db.Exec(`
		INSERT INTO pictures(path,origin,created_at) VALUES
		($1,$2,datetime('now','localtime'))
		`, newImageName, header.Filename)
		if err != nil {
			fmt.Fprintln(w, "Inserting error")
			transaction.Rollback()
			return
		}
		id, err := imageExec.LastInsertId()
		if err != nil {
			transaction.Rollback()
			fmt.Fprintln(w, "Some troubles with database")
			return
		}
		var partIds []int64
		for i := range parts {
			insertPart, err := db.Exec(`
			INSERT INTO parts(picture_id,part_num,part_path,created_at) VALUES
			($1,$2,$3,datetime('now','localtime'))`,
				id, i+1, parts[i],
			)
			if err != nil {
				fmt.Fprintf(w, "got errors in for loop")
				transaction.Rollback()
				return
			}
			partId, err := insertPart.LastInsertId()
			if err != nil {
				fmt.Fprintln(w, "error on getting last id of part")
				transaction.Rollback()
				return
			}
			partIds = append(partIds, partId)
		}
		log.Println(partIds)
		transaction.Commit()
		responseMessage := `Saved and id of picture is: ` + strconv.FormatInt(id, 10)
		for i := 0; i < len(partIds); i++ {
			responseMessage += "\n" + `Picture Part ` + strconv.Itoa(i+1) + ` id is: ` + strconv.FormatInt(partIds[i], 10)
		}
		w.Write([]byte(responseMessage))
		return
	}
	if r.Method == "GET" {
		var validID = regexp.MustCompile(`[0-9]+`)
		getParams := validID.FindAllString(r.URL.Path, -1)
		if len(getParams) == 1 {

			id := getParams[0]
			db, err := sql.Open("sqlite3", "gsc.db")
			if err != nil {
				log.Println(err)

				if err != nil {
					fmt.Fprintln(w, "Something strange happens on server")
					return
				}
				return
			}
			defer db.Close()
			var path, origin, created_at string
			err = db.QueryRow(`
			SELECT path,origin,created_at FROM pictures where id = $1
			`, id).Scan(&path, &origin, &created_at)
			log.Println(path, origin, created_at)
			file, err := os.Open(path)
			if err != nil {
				w.Write([]byte("Can't find this image"))
				return
			}
			defer file.Close()
			fileBytes, err := ioutil.ReadAll(file)
			if err != nil {
				w.Write([]byte("Cant read iamge"))
				return
			}

			w.Write(fileBytes)
			return
		} else if len(getParams) == 2 {
			db, err := sql.Open("sqlite3", "gsc.db")
			if err != nil {
				log.Println(err)

				if err != nil {
					fmt.Fprintln(w, "Something strange happens on server")
					return
				}
				return
			}
			defer db.Close()
			var path string

			err = db.QueryRow(`
			SELECT part_path FROM parts where picture_id = $1 and part_num = $2
			`, getParams[0], getParams[1]).Scan(&path)
			log.Println(path)
			file, err := os.Open(path)
			if err != nil {
				w.Write([]byte("Can't find this image"))
				return
			}
			defer file.Close()
			fileBytes, err := ioutil.ReadAll(file)
			if err != nil {
				w.Write([]byte("Cant read iamge"))
				return
			}

			w.Write(fileBytes)
			return
		}
	}
	w.Write([]byte("Sorry bad method"))
}

func getPart(w http.ResponseWriter, r *http.Request) {

	id := strings.TrimPrefix(r.URL.Path, "/images/")
	fmt.Println(id)
	w.Write([]byte("ALL FINE"))
	return

	w.Write([]byte("BAD METHOD"))
	return
}
